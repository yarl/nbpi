#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <fcntl.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>

#include <dev/i2c/i2c_io.h>

static int d;

void
dob(i2c_op_t op, uint8_t addr, uint8_t *val)
{
	i2c_ioctl_exec_t iie;

	iie.iie_op = op;
	iie.iie_addr = 0x3c >> 1;
	iie.iie_cmd = &addr;
	iie.iie_cmdlen = 1;
	iie.iie_buf = val;
	iie.iie_buflen = 1;

	if (-1 == ioctl(d, I2C_IOCTL_EXEC, &iie))
		err(EXIT_FAILURE, "ioctl");
}

void
writeb(uint8_t addr, uint8_t val)
{
	dob(I2C_OP_WRITE_WITH_STOP, addr, &val);
}

uint8_t
readb(uint8_t addr)
{
	uint8_t val;

	dob(I2C_OP_READ_WITH_STOP, addr, &val);

	return val;
}

int
main(int argc, char *argv[])
{
	uint8_t val[6] = {0,0,0,0,0,0};
	uint8_t reg[6] = {3,4,5,6,7,8};
	enum {READ, WRITE, READDATA} op;

	d = open("/dev/iic2", O_RDWR);
	if (-1 == d)
		err(EXIT_FAILURE, "open");

	i2c_ioctl_exec_t iie;

	iie.iie_op = I2C_OP_READ_WITH_STOP;;
	iie.iie_addr = 0x3c >> 1;
	iie.iie_cmd = reg;
	iie.iie_cmdlen = 6;
	iie.iie_buf = val;
	iie.iie_buflen = 6;
	
	if (-1 == ioctl(d, I2C_IOCTL_EXEC, &iie))
		err(EXIT_FAILURE, "ioctl");

	for (int i=0; i<6; ++i) {
		printf("%d\n", val[i]);
	}

	close(d);

	exit(EXIT_SUCCESS);
}

