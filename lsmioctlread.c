#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <fcntl.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>

#include "lsm303dlhc.h"

static int d;

int
main(int argc, char *argv[])
{
	enum lsm303dlhc_odr odr;

	d = open("/dev/lsm303dlhc", O_RDWR);
	if (-1 == d)
		err(EXIT_FAILURE, "open");

	if (ioctl(d, LSM303DLHC_IOCTL_RODR, &odr))
		err(EXIT_FAILURE, "ioctl");

	printf("%d\n", (int)odr);
	close(d);

	exit(EXIT_SUCCESS);
}

