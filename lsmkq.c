#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <err.h>

#include <sensor.h>

struct sensordata {
	int16_t x,y,z;
	struct timeval t;
} __packed;

void
print_md(struct sensordata* md)
{
	printf("%llds, %dus: %d %d %d\n", md->t.tv_sec, md->t.tv_usec,
			md->x, md->y, md->z);
}

int
main(int argc, char *argv[])
{
	int fd, kq, nev;
	struct kevent ev;
	struct sensordata md;
	size_t size;

	if (argc < 2)
		errx(EXIT_FAILURE, "%s dev", argv[0]);

	fd = open(argv[1], O_RDWR);
	if (-1 == fd)
		err(EXIT_FAILURE, "open");
	printf("opened\n");

	if ((kq = kqueue()) == -1)
		err(EXIT_FAILURE, "kqueue");

	EV_SET(&ev, fd,	EVFILT_READ, EV_ADD | EV_ENABLE, 0, 0, 0);
	if (kevent(kq, &ev, 1, NULL, 0, NULL))
		err(EXIT_FAILURE, "kevent");

	for (;;) {
		nev = kevent(kq, NULL, 0, &ev, 1, NULL);

		if (-1 == nev)
			err(EXIT_FAILURE, "kevent");

		printf("%lu bytes to read\n", (size_t)ev.data);
		if ((size = read(fd, &md, sizeof(md))) == -1)
			err(EXIT_FAILURE, "read (size=%lu)", size);
		if (size != sizeof(md))
			err(EXIT_FAILURE, "size(%lu) != sizeof(md)(%lu)", size, sizeof(md));

		print_md(&md);

		printf("---------------------\n");
	}

	close(fd);

	exit(EXIT_SUCCESS);
}

