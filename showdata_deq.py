#!/usr/bin/env python3.6

import fileinput
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import itertools
import collections

fig = plt.figure()
axes = fig.subplots(3,1)

NB=10
x = list(range(NB))
points = [[collections.deque([0] * NB) for i in range(3)] for i in range(3)]
lines = [ax.plot(x,np.transpose(v)) for (ax,v) in zip(axes, points)]

def init():  # only required for blitting to give a clean slate.
	for linesdev in lines:
		for line in linesdev:
			line.set_ydata([np.nan]*NB)
	for ax in axes:
		ax.set_xlim(-1, 10)
		ax.set_ylim(-1<<15,1<<15)
	return itertools.chain.from_iterable(lines)


def animate(i):
	l = i.split()
	dev = int(l.pop(0))
	for pline,line,e in zip(points[dev],lines[dev],map(int,l)):
		pline.popleft()
		pline.append(e)
		line.set_ydata(pline)
	return lines[dev]


ani0 = animation.FuncAnimation(
	fig, animate, frames=fileinput.input(), init_func=init, interval=0.1, blit=True)

plt.show()
