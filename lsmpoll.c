#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <fcntl.h>
#include <err.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>

#include "lsm303dlhc.h"

int d;

void
print_md(struct magndata* md)
{
	printf("%lld.%d: %d %d %d\n", md->t.tv_sec, md->t.tv_usec,
			md->x, md->y, md->z);
}

int
main(int argc, char *argv[])
{
	int ret;
	struct pollfd pfd[1];
	struct magndata md[3];
	ssize_t size;

	d = open("/dev/lsm303dlhc", O_RDWR);
	pfd[0].fd = d;
	if (-1 == pfd[0].fd)
		err(EXIT_FAILURE, "open");
	pfd[0].events = POLLIN | POLLRDNORM;

	size = read(d, md, sizeof(md));
	if (size != sizeof(md))
		err(EXIT_FAILURE, "read");

	printf("polling...\n");

	ret = poll(pfd, 1, INFTIM);
	if (-1 == ret)
		err(EXIT_FAILURE, "poll");
	if (0 == ret)
		err(EXIT_FAILURE, "poll timeout");

	printf("revents: %d\n", pfd[0].revents);
	if (pfd[0].revents & (POLLIN | POLLRDNORM))
		printf("data to read\n");

	close(d);

	exit(EXIT_SUCCESS);
}

