#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <fcntl.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#include <sys/ioctl.h>

#include <dev/i2c/i2c_io.h>

static int d;

void
bin(uint8_t a, char* s)
{
	int i=7;
	while (a) {
		s[i] = a%2 ? '1' : '0';
		--i;
		a >>= 1;
	}
	while (i>=0) {
		s[i] = '0';
		--i;
	}
}

void
doexec(i2c_op_t op, uint8_t devaddr, uint8_t addr, uint8_t cmdlen, uint8_t *val, uint8_t buflen)
{
	i2c_ioctl_exec_t iie;

	iie.iie_op = op;
	iie.iie_addr = devaddr;
	iie.iie_cmd = &addr;
	iie.iie_cmdlen = cmdlen;
	iie.iie_buf = val;
	iie.iie_buflen = buflen;

	if (-1 == ioctl(d, I2C_IOCTL_EXEC, &iie))
		err(EXIT_FAILURE, "ioctl");
}

void
writeb(uint8_t devaddr, uint8_t addr, uint8_t val)
{
	doexec(I2C_OP_WRITE_WITH_STOP, devaddr, addr, 1, &val, 1);
}

uint8_t
readb(uint8_t devaddr, uint8_t addr)
{
	uint8_t val;

	doexec(I2C_OP_READ_WITH_STOP, devaddr, addr, 1, &val, 1);

	return val;
}

void
readm(uint8_t devaddr, uint8_t addr, uint8_t size, uint8_t *val)
{
	doexec(I2C_OP_READ_BLOCK, devaddr, addr, 1, val, size);
}

int
main(int argc, char *argv[])
{
	uint8_t val, reg, addr, size;
	enum {READ, WRITE, READDATA} op;
	char s[9];
	s[8] = 0;
	uint8_t vals[32];

	if (argc < 3)
		errx(EXIT_FAILURE, "argc < 3");

	addr = (uint8_t)strtol(argv[1], NULL, 0);

	if(0 == strcmp(argv[2], "read")) {
		op = READ;
		if (argc != 4)
			err(EXIT_FAILURE, "argc != 4 (READ)");
	} else if (0 == strcmp(argv[2], "write")) {
		op = WRITE;
		if (argc != 5)
			err(EXIT_FAILURE, "argc != 5 (WRITE)");
	} else if (0 == strcmp(argv[2], "readdata")) {
		op = READDATA;
		if (argc != 5)
			err(EXIT_FAILURE, "argc != 6 (READDATA)");
	} else 
		err(EXIT_FAILURE, "invalid op");

	d = open("/dev/iic2", O_RDWR);
	if (-1 == d)
		err(EXIT_FAILURE, "open");

	reg = (uint8_t)strtol(argv[3], NULL, 0);
	switch (op) {
	case READ:
		val = readb(addr, reg);
		bin(val, s);
		printf("0x%x: %s(0x%x)\n", reg, s, val);
		break;
	case WRITE:
		val = (uint8_t) strtol(argv[4], NULL, 0);
		bin(val, s);
		printf("0x%x <- %s(0x%x)\n", reg, s, val);
		writeb(addr, reg, val);
		break;
	case READDATA:
		size = (uint8_t) strtol(argv[4], NULL, 0);
		readm(addr, reg, size, vals);
		for (uint8_t i=0; i<size; ++i) {
			bin(vals[i], s);
			printf("0x%x: %s(0x%x)\n", reg + i, s, vals[i]);
		}
		break;
	}

	close(d);

	exit(EXIT_SUCCESS);
}

