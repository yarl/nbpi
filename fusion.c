#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <err.h>

#include <dev/i2c/i2c_idread_io.h>

int fda, fdg, fdm;

struct sensordata {
	int16_t x,y,z;
	struct timeval t;
} __packed;

/*void
print_md(struct sensordata* md)
{
	printf("%lld,%d,%d,%d,%d\n", md->t.tv_sec, md->t.tv_usec,
			md->x, md->y, md->z);
}*/

void
print_smallmd(struct sensordata* md)
{
	printf("%d %d %d\n", md->x, md->y, md->z);
}

int
main(int argc, char *argv[])
{
	int kq, nev;
	struct kevent ev;
	struct sensordata md;
	size_t size;
	char letter;

	if (argc != 4)
		errx(EXIT_FAILURE, "%s acc gyr mag", argv[0]);

	fda = open(argv[1], O_RDWR);
	if (-1 == fda)
		err(EXIT_FAILURE, "open acc");
	fdg = open(argv[2], O_RDWR);
	if (-1 == fdg)
		err(EXIT_FAILURE, "open gyr");
	fdm = open(argv[3], O_RDWR);
	if (-1 == fdm)
		err(EXIT_FAILURE, "open mag");

	if ((kq = kqueue()) == -1)
		err(EXIT_FAILURE, "kqueue");

	EV_SET(&ev, fda, EVFILT_READ, EV_ADD, 0, 0, 0);
	if (kevent(kq, &ev, 1, NULL, 0, NULL))
		err(EXIT_FAILURE, "kevent");
	EV_SET(&ev, fdg, EVFILT_READ, EV_ADD, 0, 0, 0);
	if (kevent(kq, &ev, 1, NULL, 0, NULL))
		err(EXIT_FAILURE, "kevent");
	EV_SET(&ev, fdm, EVFILT_READ, EV_ADD|EV_ENABLE, 0, 0, 0);
	if (kevent(kq, &ev, 1, NULL, 0, NULL))
		err(EXIT_FAILURE, "kevent");

	for (;;) {
		nev = kevent(kq, NULL, 0, &ev, 1, NULL);

		if (-1 == nev)
			err(EXIT_FAILURE, "kevent");
		if ((size = read(ev.ident, &md, sizeof(md))) == -1)
			err(EXIT_FAILURE, "read");
		if (size != sizeof(md))
			err(EXIT_FAILURE, "size(%lu) != sizeof(md)(%lu)", size, sizeof(md));

		if (ev.ident == fda)
			letter = '0';
		else if (ev.ident == fdg)
			letter = '1';
		else if (ev.ident == fdm)
			letter = '2';
		else
			errx(EXIT_FAILURE, "WTF");

		printf("%c ", letter);
		print_smallmd(&md);
		fflush(stdout);
	}

	close(fda);
	close(fdg);
	close(fdm);

	exit(EXIT_SUCCESS);
}

